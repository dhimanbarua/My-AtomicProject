<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class ProfilePicture extends DB
{
    public $id="";

    public $name="";

    public $profilePicture="";

    public function __construct()
    {
        parent::__construct();
        //echo "Hello";
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('profilePicture',$data))
        {
            $this->profilePicture = $data['profilePicture'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->profilePicture);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO profilepic(name,profilePicture) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Profile Picture:$this->profilePicture]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>>[User Name: $this->name],[Profile Picture:$this->profilePicture]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3></div> ");
        }
        Utility::redirect('create.php');
    }

    public function index(){
        $STH = $this->DBH->query("SELECT * FROM profilepic WHERE is_deleted='No'");
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * FROM profilepic WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);


        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();
    public function update(){

        $arrData = array($this->name, $this->profilePicture);
        $sql = "UPDATE profilepic SET name = ?, profilePicture = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()

    public function delete(){
        $sql = "DELETE FROM profilePic WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete

    public function trash(){
        $sql = "UPDATE profilepic SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * FROM profilePic  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']))  $sql = "SELECT * FROM `profilePic` WHERE `is_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%'";
        if(isset($requestArray['byName'])) $sql = "SELECT * FROM `profilePic` WHERE `is_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()

    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `profilePic` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end

        return array_unique($_allKeywords);


    }// get all keywords


}