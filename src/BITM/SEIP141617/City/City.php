<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class City extends DB
{
    public $id="";
    public $name="";
    public $city="";



    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('city',$data))
        {
            $this->city = $data['city'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->city);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO city(name,city) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[City:$this->city]
                    <br>Data Has been Inserted Successfully!!!!!!</h3> </div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[City:$this->city]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3></div> ");
        }
        Utility::redirect('create.php');

    }

    public function index(){
        $STH = $this->DBH->query("SELECT * FROM city WHERE is_deleted='No' ORDER BY city DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }
    public function view(){

        $sql = 'SELECT * FROM city WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){

        $arrData = array ($this->name, $this->city);
        $sql = "UPDATE city SET  name = ?, city = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()

    public function delete(){
        $sql = "DELETE FROM city WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete

    public function trash(){
        $sql = "UPDATE city SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * FROM city  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byCity']) && isset($requestArray['byName']) )  $sql = "SELECT * FROM `city` WHERE `is_deleted` ='No' AND (`city` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byCity']) && !isset($requestArray['byName']) ) $sql = "SELECT * FROM `city` WHERE `is_deleted` ='No' AND `city` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byCity']) && isset($requestArray['byName']) )  $sql = "SELECT * FROM `city` WHERE `is_deleted` ='No' AND `city` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()

    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `city` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->city);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->city);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


}