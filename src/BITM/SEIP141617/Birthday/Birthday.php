<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Birthday extends DB
{
    public $id="";

    public $name="";

    public $birthday="";

    public function __construct()
    {
        parent::__construct();

    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('birthday',$data))
        {
            $this->birthday = $data['birthday'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->birthday);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO birthday(name,birthday) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'></div><h3 align='center'>[User Name: $this->name],[Birth Day:$this->birthday]
                    <br>Data Has been Inserted Successfully!!!!!!</h3> ");
        }
        else
        {
            Message::message("<div id='msg'></div><h3 align='center'>[User Name: $this->name],[Birth Day:$this->birthday]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3> ");
        }
        Utility::redirect('index.php');

    }

    public function index(){
        $STH = $this->DBH->query("SELECT * FROM birthday WHERE is_deleted='No' ORDER BY birthday DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }

    public function view(){
        $sql = 'SELECT * FROM birthday WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData = $STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $arrData = array($this->name, $this->birthday);
        $sql = "UPDATE birthday SET name = ?, birthday = ? WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }
    public function delete(){
        $sql = "DELETE FROM birthday WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete
    public function trash(){
        $sql = "UPDATE birthday SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']))  $sql = "SELECT * FROM `birthday` WHERE `is_deleted` ='No' AND (`birthday` LIKE '%".$requestArray['search']."%' OR `name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName'])) $sql = "SELECT * FROM `birthday` WHERE `is_deleted` ='No' AND `birthday` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']))  $sql = "SELECT * FROM `birthday` WHERE `is_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()

    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `birthday` WHERE `is_deleted` ='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords





}