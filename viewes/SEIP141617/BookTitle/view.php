<?php

require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;

$objBookTitle = new BookTitle();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view('obj');


?>
<html>
<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="../../../Resources/assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
        <table class="tableFull" border="3">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>BookTitle</th>
            </tr>
            <tr>
                <td><?php echo $oneData->id ?></td>
                <td><?php echo $oneData->book_title ?></td>
                <td><?php echo $oneData->author_name ?></td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
