<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\City\City;

if(!isset($_SESSION)) session_start();
echo Message::message();
$objCity = new City();
$objCity->setData($_GET);
$oneData = $objCity->view();
?>
<html>
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row-fluid user-row ">
                        <div class="book_title"><center><h2>Edit City</h2></center></div>
                        <div class="img_icon"><img src="../../../Resources/Images/ct.jpg" class="img-responsive" width="150px" height="150px" alt="Conxole Admin"/></div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="update.php" method="post" accept-charset="UTF-8" role="form" class="form-signin">
                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>
                            </label>

                            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">


                            <label>Edit Name</label><input class="form-control" name="name" value="<?php echo $oneData->name ?>"  type="text">
                            <br>
                            <select id="city" name="city" class="form-control">


                                <option <?php if($oneData->city=="Chittagong"):?>selected<?php endif ?>>Chittagong</option>
                                <option <?php if($oneData->city=="Dhaka"):?>selected<?php endif ?>>Dhaka</option>
                                <option <?php if($oneData->city=="Comilla"):?>selected<?php endif ?>>Comilla</option>
                                <option <?php if($oneData->city=="Khulna"):?>selected<?php endif ?>>Khulna</option>

                            </select>
                            <br>
                            <input class="btn btn-lg btn-success " type="submit" value="Update">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $(function() {
                $('#confirmation_message').delay(5000).fadeOut();
            });

        });
    </script>

</body>


</html>