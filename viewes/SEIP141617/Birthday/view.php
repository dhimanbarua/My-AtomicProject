<?php
require_once('../../../vendor/autoload.php');
use App\Birthday\Birthday;

$objBirthday = new Birthday();
$objBirthday->setData($_GET);
$oneData = $objBirthday->view();
?>
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row vertical-offset-100">
<table class="table-view" border="3">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Birthday</th>
    </tr>
    <tr>
        <td><?php echo $oneData->id ?></td>
        <td><?php echo $oneData->name ?></td>
        <td><?php echo $oneData->birthday ?></td>
    </tr>
</table>
        </div>
    </div>
</body>